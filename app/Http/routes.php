<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('/', 'HomeController@index');

Route::get('home', 'HomeController@index');

Route::get('info', 'InfoController@index');

Route::get('info/{crNumber}', 'InfoController@crInfo');

Route::post('info', 'InfoController@crInfo');

Route::get('list', 'ListController@index');

Route::get('timeline', 'TimelineController@index');

Route::get('timeline/{crNumber}', 'TimelineController@crTimeline');

Route::post('timeline', 'TimelineController@crTimeline');

Route::any('crSearch', function() {
    $term = Input::get('term');
    $data = DB::table("cr_master")->distinct()->select('crNumber')->where('crNumber', 'LIKE', $term . '%')->groupBy('crNumber')->take(10)->get();
    $return_array = array();
    foreach ($data as $v) {
        $return_array[] = array('value' => $v->crNumber);
    }
    return Response::json($return_array);
});

Route::any('budgetChart', function() {

    $crBudget = DB::table('cr_mgs_data')
            ->select('budgetVolume', 'budgetLineName', 'addName', 'businessCenterName')
            ->where('budgetVolume', '<>', '')
            ->first();
    $crDataAll = DB::table('cr_mgs_data')
            ->select('crNumber', 'sizingVolume', 'spentVolume', 'mspVolume', 'organizationName')
            ->where('crNumber', '<>', '')
            ->get();
    $array['sizing'] = array('name' => 'Sizing', 'type' => 'column', 'tooltip' => array('valueSuffix' => ' md'));
    $array['budget'] = array('name' => 'Budget', 'type' => 'spline', 'tooltip' => array('valueSuffix' => ' md'), 'marker' => array('enabled' => false));
    $array['spent'] = array('name' => 'Spent', 'type' => 'spline', 'tooltip' => array('valueSuffix' => ' md'));

    foreach ($crDataAll as $crData) {
        $array['sizing']['data'][] = $crData->sizingVolume;
        $array['budget']['data'][] = $crBudget->budgetVolume;
        $array['spent']['data'][] = $crData->spentVolume;
        $array['crNumber'][] = $crData->crNumber;
    }
    $returnArray = array('value' => array($array['sizing'], $array['budget'], $array['spent']), 'categories' => $array['crNumber']);
    return Response::json($returnArray);
});

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);



