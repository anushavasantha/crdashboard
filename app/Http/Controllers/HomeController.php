<?php

namespace App\Http\Controllers;

use DB;

class HomeController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Home Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index() {
        $crCountInfo = DB::table('cr_master')
                ->select(DB::raw('count(*) as count, crStatus'))
                ->groupBy('crStatus')
                ->get();

        $crTotalCount = DB::table('cr_master')
                ->select(DB::raw('count(*) as count'))
                ->first();

        $crMgsHistory = DB::table('cr_master')
                ->join('cr_history', 'cr_master.crId', '=', 'cr_history.crMasterId')
                ->select('crId', 'crNumber', 'actionName', 'actionDate', 'type', 'modifiedByUser')
                ->where('type', "MGS")
                ->orderBy('actionDate', 'desc')
                ->take(5)
                ->get();

        $crJiraHistory = DB::table('cr_master')
                ->join('cr_history', 'cr_master.crId', '=', 'cr_history.crMasterId')
                ->select('crId', 'crNumber', 'actionName', 'actionDate', 'type', 'modifiedByUser')
                ->where('type', "JIRA")
                ->orderBy('actionDate', 'desc')
                ->take(5)
                ->get();
        
        $crWinHistory = DB::table('cr_master')
                ->join('cr_history', 'cr_master.crId', '=', 'cr_history.crMasterId')
                ->select('crId', 'crNumber', 'actionName', 'actionDate', 'type', 'modifiedByUser')
                ->where('type', "WIN")
                ->orderBy('actionDate', 'desc')
                ->take(5)
                ->get();

        return view('pages/home', ['crCountInfo' => $crCountInfo, 'crTotalCount' => $crTotalCount, 'crMgsHistory' => $crMgsHistory, "crJiraHistory" => $crJiraHistory, "crWinHistory" => $crWinHistory]);
    }

}
