<?php

namespace App\Http\Controllers;

use DB;

class TimelineController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Home Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index() {
        return view('pages/timeline');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function crTimeline($crNumber = "") {
        
        if($crNumber == "")
            $crNumber = \Request::input('crNumber');

        $crWinInfo = DB::table('cr_master')
                ->select('crId')
                ->where('crNumber', $crNumber)
                ->first();

        $crHistory = DB::table('cr_history')
                ->select('actionName', 'actionDate', 'type', 'modifiedByUser')
                ->where('crMasterId', $crWinInfo->crId)
                ->orderBy('actionDate', 'desc')
                ->get();

        return view('pages/timeline', ['crNumber' => $crNumber, 'crHistory' => $crHistory]);
    }

}
