<?php

namespace App\Http\Controllers;

use DB;
use App\Cr;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ListController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Home Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index() {
        $crs = DB::table('cr_master')
            ->join('cr_win_data', 'cr_master.crId', '=', 'cr_win_data.crMasterId')   
            ->select('crNumber', 'crTitle', 'crStatus', 'crOrgName')
            ->get();
            
        return view('pages/list', compact('crs'));
    }

}
