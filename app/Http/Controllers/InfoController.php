<?php

namespace App\Http\Controllers;

use DB;

class InfoController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Home Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index() {
        return view('pages/info');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function crInfo($crNumber = "") {

        if ($crNumber == "")
            $crNumber = \Request::input('crNumber');

        $crWinInfo = DB::table('cr_master')
                ->join('cr_win_data', 'cr_master.crId', '=', 'cr_win_data.crMasterId')
                ->select('crId', 'crNumber', 'crTitle', 'crStatus', 'crOrgName', 'crAddProject', 'crAddProduct', 'crBudgetValue', 'crAssignedTo')
                ->where('crNumber', $crNumber)
                ->first();
        
        $crWinSizing = DB::table('cr_win_sizing')
                ->select('sizingOrgName', 'sizingActivityName', 'sizingValue', 'oomValue')
                ->where('crMasterId', $crWinInfo->crId)
                ->get();

        $crMgntSizing = DB::table('mgs_sizings')
                ->select('SizingId', 'orgName', 'sizingStatus', 'value', 'valueOOM', 'taskId')
                ->where('crMasterId', $crWinInfo->crId)
                ->get();

        $i = 0;
        foreach ($crMgntSizing as $sizing) {
            $crMgntSizingDetails = DB::table('mgs_sizingdetails')
                    ->select('catalogName', 'activityName', 'value', 'isOOM')
                    ->where('SizingID', $sizing->SizingId)
                    ->get();
            $crMgntSizing[$i]->sizingDetails = $crMgntSizingDetails;
            $i++;
        }

        $i = 0;
        foreach ($crMgntSizing as $sizing) {
            $crMgntSizingTaxes = DB::table('mgs_sizingtaxes')
                    ->select('taxName', 'activityName', 'value', 'isOOM')
                    ->where('SizingID', $sizing->SizingId)
                    ->get();
            $crMgntSizing[$i]->sizingTaxes = $crMgntSizingTaxes;
            $i++;
        }

        $crJiraInfo = DB::table('cr_jira_master')
                ->select('jirakey', 'projectname', 'description', 'estimated', 'remaining', 'logged')
                ->where('crMasterId', $crWinInfo->crId)
                ->first();
        
        $crMspInfo = DB::table('mgs_milestones')
                ->select('milestoneName', 'mspFinishDate', 'mgsInitialDate')
                ->where('crMasterId', $crWinInfo->crId)
                ->get();
  
        return view('pages/info', ['crNumber' => $crNumber, 'crWinInfo' => $crWinInfo, 'crWinSizing' => $crWinSizing, 'crMgntSizing' => $crMgntSizing, 'crJiraInfo' => $crJiraInfo, 'crMspInfo' => $crMspInfo]);
    }

}
