<!-- start: Main Menu -->
<div id="sidebar-left" class="span2">
    <div class="nav-collapse sidebar-nav">
        <ul class="nav nav-tabs nav-stacked main-menu">
            <li class="{{ (Request::is('/') ? 'active' : '') }}"><a href="{!! URL::to('') !!}"><i class="icon-dashboard"></i><span class="hidden-tablet"> HOME</span></a></li>	            
            <li class="{{ (Request::is('info') ? 'active' : '') }}"><a href="{!! URL::to('info') !!}"><i class="icon-star"></i><span class="hidden-tablet"> CR INFO</span></a></li>
            <li class="{{ (Request::is('list') ? 'active' : '') }}"><a href="{!! URL::to('list') !!}"><i class="icon-align-justify"></i><span class="hidden-tablet"> CR LIST</span></a></li>
            <li class="{{ (Request::is('timeline') ? 'active' : '') }}"><a href="{!! URL::to('timeline') !!}"><i class="icon-eye-open"></i><span class="hidden-tablet"> CR TIMELINE</span></a></li>
        </ul>
    </div>
</div>
<!-- end: Main Menu -->