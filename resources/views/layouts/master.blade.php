<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- start: Meta -->
        <meta charset="utf-8">
        <title>@yield('title')</title>
        <meta name="description" content="CR Dashboard">
        <meta name="author" content="Praveen, Yerris, Anusha">        
        <!-- end: Meta -->

        <!-- start: Mobile Specific -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- end: Mobile Specific -->

        <!-- start: CSS -->
        <link id="bootstrap-style" href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/bootstrap-responsive.min.css') }}" rel="stylesheet">
        <link id="base-style" href="{{ asset('css/style.css') }}" rel="stylesheet">
        <link id="base-style-responsive" href="{{ asset('css/style-responsive.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
        <!-- end: CSS -->
        <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
                <link id="ie-style" href="{{ asset('css/ie.css') }}" rel="stylesheet">
        <![endif]-->
        <!--[if IE 9]>
                <link id="ie9style" href="{{ asset('css/ie9.css') }}" rel="stylesheet">
        <![endif]-->
    </head>
    <body>

        @include('includes.header')

        <div class="container-fluid-full">
            <div class="row-fluid">

                @include('includes.sidebar')
                @yield('content')

            </div><!--/#content.span10-->
        </div><!--/fluid-row-->

        <div class="modal hide fade" id="myModal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">�</button>
                <h3>Settings</h3>
            </div>
            <div class="modal-body">
                <p>Here settings can be configured...</p>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn" data-dismiss="modal">Close</a>
                <a href="#" class="btn btn-primary">Save changes</a>
            </div>
        </div>
        <div class="clearfix"></div>
        @include('includes.footer')
        <!-- start: JavaScript-->
        <script src="{{ asset('js/jquery-1.9.1.min.js') }}"></script>
        <script src="{{ asset('js/jquery-migrate-1.0.0.min.js') }}"></script>
        <script src="{{ asset('js/jquery-ui-1.10.0.custom.min.js') }}"></script>
        <script src="{{ asset('js/jquery.ui.touch-punch.js') }}"></script>
        <script src="{{ asset('js/modernizr.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/jquery.cookie.js') }}"></script>
        <script src="{{ asset('js/fullcalendar.min.js') }}"></script>
        <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('js/excanvas.js') }}"></script>
        <script src="{{ asset('js/jquery.flot.js') }}"></script>
        <script src="{{ asset('js/jquery.flot.pie.js') }}"></script>
        <script src="{{ asset('js/jquery.flot.stack.js') }}"></script>
        <script src="{{ asset('js/jquery.flot.resize.min.js') }}"></script>
        <script src="{{ asset('js/jquery.chosen.min.js') }}"></script>
        <script src="{{ asset('js/jquery.uniform.min.js') }}"></script>
        <script src="{{ asset('js/jquery.cleditor.min.js') }}"></script>
        <script src="{{ asset('js/jquery.noty.js') }}"></script>
        <script src="{{ asset('js/jquery.elfinder.min.js') }}"></script>
        <script src="{{ asset('js/jquery.raty.min.js') }}"></script>
        <script src="{{ asset('js/jquery.iphone.toggle.js') }}"></script>
        <script src="{{ asset('js/jquery.uploadify-3.1.min.js') }}"></script>
        <script src="{{ asset('js/jquery.gritter.min.js') }}"></script>
        <script src="{{ asset('js/jquery.imagesloaded.js') }}"></script>
        <script src="{{ asset('js/jquery.masonry.min.js') }}"></script>
        <script src="{{ asset('js/jquery.knob.modified.js') }}"></script>
        <script src="{{ asset('js/jquery.sparkline.min.js') }}"></script>
        <script src="{{ asset('js/counter.js') }}"></script>
        <script src="{{ asset('js/retina.js') }}"></script>
        <script src="{{ asset('js/custom.js') }}"></script>
        @yield('javascript')
        <!-- end: JavaScript-->
    </body>
</html>
