@extends('layouts.master')
@section('title', 'CR Dashboard - List')
@section('content')
<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="home">HOME</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#">LIST</a></li>
    </ul>

    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Members</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>                           
                            <th>CR Number</th>
                            <th>CR Title</th>
                            <th>CR Org</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>   
                    <tbody>
                        @foreach ($crs as $cr)
                        <tr>                           
                            <td class="center">{{ $cr->crNumber }}</td>
                            <td class="center">{{ $cr->crTitle }}</td>
                            <td class="center">{{ $cr->crOrgName }}</td>
                            <td class="center">
                                @if ($cr->crStatus == "Opened")
                                    <?php $statusLabel = "-success"; ?>
                                @elseif ($cr->crStatus == "Assigned")
                                    <?php $statusLabel = "-important"; ?>
                                @elseif ($cr->crStatus == "Confirmed")
                                    <?php $statusLabel = "-warning"; ?>
                                @else
                                    <?php $statusLabel = ""; ?>
                                @endif
                                <span class="label label{{ $statusLabel }}">{{ $cr->crStatus }}</span>
                            </td>
                            <td class="center">
                                <a class="btn btn-success" href="/info/{{$cr->crNumber}}">
                                    <i class="halflings-icon white zoom-in"></i>  
                                </a>   
                                <a class="btn btn-success" href="/timeline/{{ $cr->crNumber }}">
                                    <i class="halflings-icon white time"></i>  
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>            
            </div>
        </div><!--/span-->

    </div><!--/row-->


</div><!--/.fluid-container-->

<!-- end: Content -->
@stop
