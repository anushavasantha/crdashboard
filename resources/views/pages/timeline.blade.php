@extends('layouts.master')
@section('title', 'CR Dashboard - Timeline')
@section('content')
<!-- start: Content -->
<div id="content" class="span10">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="home">HOME</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#">TIMELINE</a></li>
    </ul>

    <div class="row-fluid">	
        <div id="form-container">
            {!! Form::open(array('url' => 'timeline', 'method' => 'post', 'id' => 'crSearchForm')) !!}    
            <a class="search-submit-button" href="javascript:void(0)" id="crSearchButton">
                <span class="glyphicons-icon search crSearchIcon"></span>
            </a>
            <div id="searchtext"> 
                {!! Form::text('crNumber', @$crNumber, ['required', 'id' => 'crNumber', 'placeholder' => 'Enter a CR Number']) !!}
            </div>
            {!! Form::close() !!}
        </div>
        <div class="clearfix"></div>
    </div><!--/row-->

    @if (isset($crNumber) && $crNumber != "") 
    <div class="row-fluid">                        
        <div class="span12 noMarginLeft">
            <div class="timeline">
                <?php $i = 0; ?>
                @foreach ($crHistory as $history) 
                @if($i % 2 == 0) 
                <?php $alt = ""; $wow = " wow bounceInLeft"; ?>                
                @else 
                <?php $alt = "alt"; $wow = " wow bounceInRight"; ?>       
                @endif    
                <div class="timeslot {{$alt}}">
                    <div class="task {{$wow}}">
                        <span>                            
                            <span class="details">
                                {!! $history->actionName !!}
                            </span>
                            <span>                                
                                <span class="remaining">
                                    {{$history->modifiedByUser}}
                                </span>	
                            </span> 
                        </span>
                        <div class="arrow"></div>
                    </div>
                    <div class="icon">
                        @if($history->type == "WIN") 
                        <img src="{{ asset('img/win.png')}}" title="WIN@" />     
                        @elseif($history->type == "MGS") 
                        <img src="{{ asset('img/ms.png')}}" title="Mgnt Suite" />
                        @else
                        <img src="{{ asset('img/jira.png')}}" title="Jira" />
                        @endif 
                    </div>
                    <div class="time">
                        {{$history->actionDate}}
                    </div>
                </div>
                <?php $i++; ?>
                @endforeach
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    @endif
</div><!--/.fluid-container-->

<!-- end: Content -->
@stop

@section('javascript')
<script src="{{ asset('js/wow.min.js') }}"></script>
<script>
$(function ()
{
    $("#crNumber").autocomplete({
        autoFocus: true,
        source: "{{url('crSearch')}}",
        minLength: 1,
        select: function (event, ui) {
            $('#q').val(ui.item.value);
        }
    });

    $("#crSearchButton").click(function () {
        $("#crSearchForm").submit();
    });

    wow = new WOW(
            {
                animateClass: 'animated'
            }
    );
    wow.init();
});
</script>
@stop