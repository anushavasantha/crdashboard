@extends('layouts.master')
@section('title', 'CR Dashboard - Info')
@section('content')
<!-- start: Content -->
<div id="content" class="span10">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="home">HOME</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#">INFO</a></li>
    </ul>  
    <div class="row-fluid">	
        <div id="form-container">
            {!! Form::open(array('url' => 'info', 'method' => 'post', 'id' => 'crSearchForm')) !!}    
            <a class="search-submit-button" href="javascript:void(0)" id="crSearchButton">
                <span class="glyphicons-icon search crSearchIcon"></span>
            </a>
            <div id="searchtext"> 
                {!! Form::text('crNumber', @$crNumber, ['required', 'id' => 'crNumber', 'placeholder' => 'Enter a CR Number']) !!}
            </div>
            {!! Form::close() !!}
        </div>
        <div class="clearfix"></div>
    </div><!--/row-->

    @if (isset($crNumber) && $crNumber != "") 
    <div class="row-fluid">	

        <div class="box span6" onTablet="span6" onDesktop="span6">
            <div class="box-header">
                <h2><i class="halflings-icon list"></i><span class="break"></span>WIN@ Info</h2>
                <div class="box-icon">
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <table class="table table-bordered table-striped">
                    <tbody><tr>
                            <td><b>Title</b></td>
                            <td>
                                {{ $crWinInfo->crTitle }}
                            </td>
                        </tr>
                        <tr>
                            <td><b>Status</b></td>
                            <td>
                                {{ $crWinInfo->crStatus }}
                            </td>
                        </tr>
                        <tr>
                            <td><b>Org</b></td>
                            <td>
                                {{ $crWinInfo->crOrgName }}
                            </td>
                        </tr>
                        <tr>
                            <td><b>Add Project</b></td>
                            <td>
                                {{ $crWinInfo->crAddProject }}
                            </td>
                        </tr>
                        <tr>
                            <td><b>Add Product</b></td>
                            <td>
                                {{ $crWinInfo->crAddProduct }}
                            </td>
                        </tr>
                        <tr>
                            <td><b>Budget Value</b></td>
                            <td>
                                {{ $crWinInfo->crBudgetValue }}
                            </td>
                        </tr>
                        <tr>
                            <td><b>Assigned To</b></td>
                            <td>
                                {{ $crWinInfo->crAssignedTo }}
                            </td>
                        </tr>
                    </tbody></table>
                <h2>Sizing Details</h2>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Org</th>
                            <th>Activity</th>
                            <th>Sizing</th>
                            <th>OOM</th>                                          
                        </tr>
                    </thead>                      
                    <tbody>
                        @foreach ($crWinSizing as $sizing)
                        <tr>
                            <td>{{ $sizing->sizingOrgName }}</td>
                            <td>{{ $sizing->sizingActivityName }}</td>
                            <td> @if($sizing->sizingValue != "") {{ $sizing->sizingValue }} @else {{ 0 }} @endif</td>  
                            <td> @if($sizing->oomValue != "") {{ $sizing->oomValue }} @else {{ 0 }} @endif</td>                                       
                        </tr>  
                        @endforeach
                    </tbody>
                </table>
                <a href="http://aproach.muc.amadeus.net/NotesLink/nl?RNID={{$crNumber}}" class="btn btn-primary pull-right" target="_blank">WIN@</a>
                <div class="clearfix"></div>
            </div>
        </div><!--/span-->

        <div class="box span6" onTablet="span6" onDesktop="span6">
            <div class="box-header">
                <h2><i class="halflings-icon list"></i><span class="break"></span>Mgnt Suite Info</h2>
                <div class="box-icon">
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <table class="table table-bordered table-striped">  
                    <thead>
                        <tr>
                            <th>Org</th>
                            <th>Status</th>
                            <th>Sizing</th>
                            <th>OOM</th>
                        </tr>
                    </thead> 
                    <tbody>    
                        <?php $i = 1; ?>
                        @if ($crMgntSizing != null)
                        @foreach ($crMgntSizing as $sizing)
                        <tr>
                            <td data-toggle="modal" data-target="#myModal{{$i}}"><i class="halflings-icon info-sign"></i> {{ $sizing->orgName }}</td>
                            <td>{{ $sizing->sizingStatus }}</td>
                            <td> @if($sizing->value != "") {{ $sizing->value }} @else {{ 0 }} @endif</td>  
                            <td> @if($sizing->valueOOM != "") {{ $sizing->valueOOM }} @else {{ 0 }} @endif</td>                             
                        </tr> 
                        <?php $i++; ?>
                        @endforeach
                        @endif
                    </tbody>
                </table>   
                @if ($crMgntSizing != null)
                <a href="http://ncebmsprd.nce.amadeus.net/SizingApplication/events/tk/actions/taskForm.php?TID={{$sizing->taskId}}" class="btn btn-primary pull-right" target="_blank">Mgnt Suite</a>
                <div class="clearfix"></div>
                @endif
            </div>
        </div><!--/span-->
    </div>    
    <?php $i = 1; ?>
    @foreach ($crMgntSizing as $sizing)
    <!-- Modal -->
    <div class="modal fade" id="myModal{{$i}}" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content infoTable">
                <h2>Direct Cost</h2>
                <table class="table table-bordered table-striped">  
                    <thead>
                        <tr>       
                            <th>Catalog Name</th>
                            <th>Activity Name</th>
                            <th>Value</th>
                            <th>Is OOM</th>
                        </tr>
                    </thead> 
                    <tbody>   

                        @foreach ($sizing->sizingDetails as $sizingDetail)
                        <tr>   
                            <td>{{ $sizingDetail->catalogName }}</td>
                            <td>{{ $sizingDetail->activityName }}</td>
                            <td> @if($sizingDetail->value != "") {{ $sizingDetail->value }} @else {{ 0 }} @endif</td>  
                            <td> @if($sizingDetail->isOOM == 0)  No  @else Yes @endif</td>                             
                        </tr> 

                        @endforeach
                    </tbody>
                </table>
                <h2>In-Direct Cost</h2>
                <table class="table table-bordered table-striped">  
                    <thead>
                        <tr>   
                            <th>Catalog Name</th>
                            <th>Activity Name</th>
                            <th>Sizing</th>
                            <th>OOM</th>
                        </tr>
                    </thead> 
                    <tbody>                          
                        @foreach ($sizing->sizingTaxes as $sizingTax)
                        <tr>   
                            <td>{{ $sizingTax->taxName }}</td>
                            <td>{{ $sizingTax->activityName }}</td>
                            <td> @if($sizingTax->value != "") {{ $sizingTax->value }} @else {{ 0 }} @endif</td>  
                            <td> @if($sizingTax->isOOM != "") No @else Yes @endif</td>                             
                        </tr>                         
                        @endforeach
                    </tbody>
                </table>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <?php $i++; ?>
    @endforeach

    <div class="row-fluid">
        <div class="box span6" onTablet="span6" onDesktop="span6">
            <div class="box-header">
                <h2><i class="halflings-icon list"></i><span class="break"></span>Jira Info</h2>
                <div class="box-icon">
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                @if ($crJiraInfo != null)
                <table class="table table-bordered table-striped">
                    <tbody><tr>
                            <td><b>Jira Key</b></td>
                            <td>
                                {{ $crJiraInfo->jirakey }}
                            </td>
                        </tr>
                        <tr>
                            <td><b>Project Name</b></td>
                            <td>
                                {{ $crJiraInfo->projectname }}
                            </td>
                        </tr>
                        <tr>
                            <td><b>Description</b></td>
                            <td>
                                {{ $crJiraInfo->description }}
                            </td>
                        </tr>
                        <tr>
                            <td><b>Estimated</b></td>
                            <td>
                                {{ $crJiraInfo->estimated }}
                            </td>
                        </tr>
                        <tr>
                            <td><b>Remaining</b></td>
                            <td>
                                {{ $crJiraInfo->remaining }}
                            </td>
                        </tr>
                        <tr>
                            <td><b>Logged</b></td>
                            <td>
                                {{ $crJiraInfo->logged }}
                            </td>
                        </tr>                       
                    </tbody>
                </table>
                <a href="http://rndwww.nce.amadeus.net/agile-staging/browse/{{ $crJiraInfo->jirakey }}" class="btn btn-primary pull-right" target="_blank">Jira</a>
                @endif
                <div class="clearfix"></div>
            </div>
        </div><!--/span-->

        <div class="box span6" onTablet="span6" onDesktop="span6">
            <div class="box-header">
                <h2><i class="halflings-icon list"></i><span class="break"></span>MSP Info</h2>
                <div class="box-icon">
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">

                <table class="table table-bordered table-striped">  
                    <thead>
                        <tr>
                            <th>Milestone Name</th>
                            <th>Finish Date</th>
                            <th>Initial Date</th>                            
                        </tr>
                    </thead> 
                    <tbody>   
                        @if ($crMspInfo != null)
                        @foreach ($crMspInfo as $mspInfo)
                        <tr>
                            <td>{{ $mspInfo->milestoneName }}</td>
                            <td>{{ $mspInfo->mspFinishDate }}</td>
                            <td>{{ $mspInfo->mgsInitialDate }}</td>                                                        
                        </tr>                         
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div><!--/span-->
    </div>
    @endif
</div><!--/.fluid-container-->

<!-- end: Content -->
@stop

@section('javascript')
<script>
    $(function ()
    {
        $("#crNumber").autocomplete({
            autoFocus: true,
            source: "{{url('crSearch')}}",
            minLength: 1,
            select: function (event, ui) {
                $('#q').val(ui.item.value);
            }
        });

        $("#crSearchButton").click(function () {
            $("#crSearchForm").submit();
        });
    });
</script>
@stop
