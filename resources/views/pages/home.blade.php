@extends('layouts.master')
@section('title', 'CR Dashboard - Home')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="home">HOME</a> 
            <i class="icon-angle-right"></i>
        </li>
    </ul>
    <div id="budgetContainer" style="min-width: 400px; height: 400px; margin: 0 auto; padding-bottom: 20px;"></div>

    <div class="row-fluid hideInIE8 circleStats">
        @foreach($crCountInfo as $countInfo)
        <?php $percent = ($countInfo->count / $crTotalCount->count) * 100; ?>
        @if ($countInfo->crStatus == "Confirmed")
        <?php $statusLabel = "yellow"; ?>
        @elseif ($countInfo->crStatus == "Assigned")
        <?php $statusLabel = "red"; ?>
        @elseif ($countInfo->crStatus == "Loaded to Test")
        <?php $statusLabel = "green"; ?>
        @elseif($countInfo->crStatus == "Opened")
        <?php $statusLabel = "pink"; ?>
        @elseif($countInfo->crStatus == "Implemented")
        <?php $statusLabel = "greenDark"; ?>
        @else
        <?php $statusLabel = "greenLight"; ?>
        @endif
        <div class="span2" onTablet="span4" onDesktop="span2">
            <div class="circleStatsItemBox {{ $statusLabel }}">
                <div class="header">{{ $countInfo->crStatus }}</div>
                <span class="percent">percent</span>
                <div class="circleStat">
                    <input type="text" value="{{ $percent }}" class="whiteCircle" />
                </div>		
                <div class="footer">
                    <span class="count">
                        <span class="number">{{ $countInfo->count }}</span>
                        <span class="unit">CR</span>
                    </span>
                    <span class="sep"> / </span>
                    <span class="value">
                        <span class="number">{{ $crTotalCount->count }}</span>
                        <span class="unit">CR</span>
                    </span>	
                </div>
            </div>
        </div>     
        @endforeach
    </div>		

    <div class="row-fluid">

        <div class="box black span4" onTablet="span6" onDesktop="span4">
            <div class="box-header">
                <h2><i class="halflings-icon white user"></i><span class="break"></span>Recent Updates - MSuite</h2>
                <div class="box-icon">
                    <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <ul class="dashboard-list metro">
                    <?php $i = 0; ?>
                    @foreach($crMgsHistory as $mgsHistory)
                    @if($i % 2 == 0) 
                    <?php $alt = "textGrey"; ?>                
                    @else 
                    <?php $alt = "blue"; ?>       
                    @endif    
                    <li style="min-height: 50px;" class="{{$alt}}">
                        <img class="avatar"  src="img/default.png">
                        <b>CR: {{$mgsHistory->crNumber}}</b><br />
                        <b><span data-rel="tooltip" data-original-title="{{$mgsHistory->actionName}}">{{Str::limit($mgsHistory->actionName, 200)}}...</span></b><br />
                        <b>on {{$mgsHistory->actionDate}}, by {{$mgsHistory->modifiedByUser}}</b>
                    </li>  
                    <?php $i++; ?>
                    @endforeach
                </ul>
            </div>
        </div><!--/span-->

        <div class="box black span4" onTablet="span6" onDesktop="span4">
            <div class="box-header">
                <h2><i class="halflings-icon white user"></i><span class="break"></span>Recent Updates - Jira</h2>
                <div class="box-icon">
                    <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <ul class="dashboard-list metro">
                    <?php $i = 0; ?>
                    @foreach($crJiraHistory as $jiraHistory)
                    @if($i % 2 == 0) 
                    <?php $alt = "textGrey"; ?>                
                    @else 
                    <?php $alt = "greenLight"; ?>       
                    @endif    
                    <li style="min-height: 50px;" class="{{$alt}}">
                        <img class="avatar" src="img/default.png">
                        <b>CR: {{$jiraHistory->crNumber}}</b><br />   
                        <b><span data-rel="tooltip" data-original-title="<?php echo str_replace("<br/>", " ", $jiraHistory->actionName);?>"><?php echo substr(str_replace("<br/>", " ", $jiraHistory->actionName), 0, 75)."...";?></span></b><br />
                        <b>on {{$jiraHistory->actionDate}}, by {{$jiraHistory->modifiedByUser}}</b>
                    </li>  
                    <?php $i++; ?>
                    @endforeach
                </ul>
            </div>
        </div><!--/span-->

        <div class="box black span4" onTablet="span6" onDesktop="span4">
            <div class="box-header">
                <h2><i class="halflings-icon white user"></i><span class="break"></span>Recent Updates - Win@</h2>
                <div class="box-icon">
                    <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <ul class="dashboard-list metro">     
                    <?php $i = 0; ?>
                    @foreach($crWinHistory as $winHistory)
                    @if($i % 2 == 0) 
                    <?php $alt = "textGrey"; ?>                
                    @else 
                    <?php $alt = "yellow"; ?>       
                    @endif    
                    <li style="min-height: 50px;" class="{{$alt}}">
                        <img class="avatar" src="img/default.png">
                        <b>CR: {{$winHistory->crNumber}}</b><br />   
                        <b><span data-rel="tooltip" title="" href="#" data-original-title="{{$winHistory->actionName}}">{{Str::limit($winHistory->actionName, 200)}}...</span></b><br />
                        <b>on {{$winHistory->actionDate}}, by {{$winHistory->modifiedByUser}}</b>
                    </li>  
                    <?php $i++; ?>
                    @endforeach
                </ul>
            </div>
        </div><!--/span-->
    </div>
</div><!--/.fluid-container-->

<!-- end: Content -->
@stop

@section('javascript')
<script src="{{ asset('js/highcharts.js') }}"></script>
<script src="{{ asset('js/exporting.js') }}"></script>
<script src="{{ asset('js/home_init.js') }}"></script>
@stop
